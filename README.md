# Delta-Approximation


We consider the mixed logit assortment problem under continuously distributed random parameters (random parameters logit). This problem is known to be NP-complete and approximation guarantees based on the revenue-ordered assortment exist. The revenue-ordered assortment solely contains products with the highest revenue. Using concentration inequalities, we propose upper bounds for the approximation guarantee. These bounds depend on the distribution of the customers' utility function (random parameters). Our results show that the approximation to the optimal solution by revenue-ordered assortments may be questionable.

